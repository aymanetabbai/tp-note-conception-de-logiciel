# TP noté conception de logiciel(Random_music_generator)
Le but de l'application est de créer une API qui communique avec les APIs  AudioDB
et LyricsOVH, pour génerer une playlist(20 chansons) aléatoire d'un artiste issu d'un json.

## Schéma  d'architecture 

```mermaid
graph TD;
  A[Client]-->|http|B[Serveur];
 B -->|https| C[LyricsOVH];
  B -->|https| D[AudioDB];
```

## Installation
Installer les dépendances dans le fichier requirements.txt
`pip install -r serveur/requirements.txt`

 ## lancer le serveur 

`python3 server/main.py`

 ## lancer l'application dans un autre terminal

`python3 client/main.py`



## Resultats

retourne un liste de dictionnaire 


## Test : avec 

`python -m unittest test.py`

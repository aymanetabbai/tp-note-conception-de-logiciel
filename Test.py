
from unittest import TestCase
from serveur.main import *
from client.main import *

class Test(TestCase):
    def testGet_id_artist(self):
        #given
        id_artist="112884"
        #when
        get_id_artist=get_id_artist_by_name("rick astley")
        #then
        self.assertEqual(id_artist,get_id_artist)
    

    
    def test_generate_playlist(self) :
        #given
        n_song=20
        #when
        n_get_song=len(generate_playlist())
        #then
        self.assertEqual(n_song,n_get_song)
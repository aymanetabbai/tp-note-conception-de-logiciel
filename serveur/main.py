from typing import Optional
from fastapi import FastAPI
from dotenv import load_dotenv
import uvicorn
import os
import random
import requests
import json

#AudioDB = os.environ.get("AudioDB")
#Lyricsovh  = os.environ.get("Lyricsovh")
searchArtist = "search.php?s="
searchAlbum= "album.php?i="
SearchTrack = "track.php?m="
Lyricsovh  = os.environ.get("Lyricsovh")
app = FastAPI()
AudioDB="https://www.theaudiodb.com/api/v1/json/2/"
Lyricsovh="https://api.lyrics.ovh/v1/"

@app.get("/")
def Check_api():
    """
    fonction qui teste le bon fonctionnement de l'API

    Returns:
        dictionnaire 
    """
    


    url_AudioDB=AudioDB + searchArtist + "rick astley"
    rAudioDB = requests.get(url_AudioDB)
    url_Lyrics=Lyricsovh + "Rick Astley" + "/" + "Never Gonna Give You Up"
    rLyrics = requests.get(url_Lyrics)
   
    return {"AudioDB_status_code":rAudioDB.status_code,"Lyricsovh_status_code": rLyrics.status_code}

def get_id_artist_by_name(artist_name : str):
       """
       fonction qui retourne id d'un artiste à partir de son nom

       Returns:
       str
       
       """
       url=AudioDB+searchArtist
       try:
           requete=requests.get(url+artist_name)
           data = requete.json()
           id_artist=data['artists'][0]['idArtist']
           return id_artist
       except:
         return("Artiste introuvable")

def get_album_artist_by_id(id_artist:int):
    """
       fonction qui retourne id d'album d'un artiste à partir de son id

       Returns:
       str
       
       """
    url=AudioDB+searchAlbum
    try:
           requete=requests.get(url+str(id_artist))
           data = requete.json()
           id_album=data['album'][0]['idAlbum']
           return id_album
    except:
        return("Album introuvable")
  
def get_track_artist_by_id_album(id_album:int):
    """
       fonction qui retourne les details d'un track  d'un artiste à partir de l'id de l'album"

       Returns:

       dictionnaire 
       
       """
    url=AudioDB+SearchTrack
    try:

        requete=requests.get(url+str(id_album))
        data = requete.json()
        tracks=data['track']
        track = random.choice(tracks)
        strArtist = track["strArtist"]
        title = track["strTrack"]

        if track["strMusicVid"]:

            suggested_youtube_url = track["strMusicVid"]
                
        else:

            suggested_youtube_url="lien introuvable"
        return {"artist" : strArtist, "title" : title, "suggested_youtube_url" : suggested_youtube_url}
    except:
        return "track introuvable"


def get_lyrics(artist_name :str,title : str):
    """
       fonction qui retourne lyrics  d'un artiste à partir dy nom d'artiste et le titre "

       Returns:

       str 
       
       """
    try:

        url=Lyricsovh + artist_name + "/" + title
        r=requests.get(url) 
        data=r.json()
        return data["lyrics"]
    except:
        return("Lyrics introuvable")

@app.get("/random/{artist_name}")
def get_summary_music(artist_name:str):
         id_artist=get_id_artist_by_name(artist_name)
         if id_artist!="Artiste introuvable":
            id_album=get_album_artist_by_id(id_artist)
            if id_album!="Album introuvable":
               track=get_track_artist_by_id_album(id_album)
               lyrics=get_lyrics(artist_name,track["title"])
               if lyrics!="Lyrics introuvable":
                    track["lyrics"]=lyrics
               else:
                   track["lyrics"]="Lyrics introuvable"
               return track
            else:
                return id_album
         else:
             return id_artist


 


if __name__ == "__main__":

    uvicorn.run("main:app", host="127.0.0.1", port=8000, log_level="info")
